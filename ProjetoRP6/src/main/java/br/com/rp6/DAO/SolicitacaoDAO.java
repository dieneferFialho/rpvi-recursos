/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.com.rp6.DAO;

import br.com.rp6.authentication.User;
import br.com.rp6.model.Solicitacao;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author MichelM
 */
public class SolicitacaoDAO extends ConFactory {

    public void salvar(Solicitacao solicitacao) {
        
        try {

            conectar();
            System.out.println(""+solicitacao.getItens().size());
            for (int i = 0; solicitacao.getItens().size() > i; i++) {
                String sql = null;
                System.out.println(" entrou no for");
                sql += "INSERT INTO public.solicitacao(\n"
                        + "            numero, \"codigoItem\", \"codigoAlmContem\", \"codAlmDestino\", date, \n"
                        + "            status)\n"
                        + "    VALUES ('" + solicitacao.getNumero() + "','" + solicitacao.getItens().get(i).getCodigo() + "','" + solicitacao.getItens().get(i).getAlmoxarifado() + "', '" + solicitacao.getUsuario().getCodigo() + "', '" + solicitacao.getData() + "', \n"
                        + "            'solicitado');";
                System.out.println(""+sql);
                rs = stm.executeQuery(sql);
            }
            
            fechar();
             System.out.println("2========"+solicitacao.getItens().size());
        } catch (Exception e) {
            System.out.println(solicitacao.getData());
            System.out.println(solicitacao.getData());
        }

    }

//    public void salvar(Solicitacao soli) {
//        try {
//            conectar();
//            
//            for (int i = 0; i < soli.size(); i++) {
//                String inclui = "INSERT INTO public.solicitacao(\n"
//                        + "              data, status, id_item, quant, id_solicitante, \n"
//                        + "            id_destino)\n"
//                        + "    VALUES ( '07/11/1993', 'solicitado', 2, 1, 2, \n"
//                        + "            3);";
//                stm.executeUpdate(inclui);
//            }
//
//        } catch (SQLException ex) {
//            Logger.getLogger(SolicitacaoDAO.class.getName()).log(Level.SEVERE, null, ex);
//        }
//
//    }
    public ArrayList<Solicitacao> buscarSolicitacoes() {

        ArrayList<Solicitacao> lista = new ArrayList<>();

        User user = new User();

        Integer cod = user.getUsuarioLogado().getCodAlm();
        String status = "aprovado";

        try {
            conectar();
            String sql = "SELECT        *          \n"
                    + "                                   FROM \n"
                    + "                                         public.solicitacao\n"
                    + "                    WHERE\n"
                    + "                                          solicitacao.\"codAlmDestino\" =  '" + cod + "' and\n"
                    + "                                          solicitacao.\"status\" =  '" + status + "' ;";

            rs = stm.executeQuery(sql);
            Solicitacao soli = new Solicitacao();

            while (rs.next()) {

                soli.setNumero(rs.getInt("codAlmDestino"));
                soli.setData(rs.getDate("date"));
                soli.setStatus(rs.getString("status"));
                lista.add(soli);
                System.out.println(soli.getNumero());
            }
            fechar();
            return lista;
        } catch (SQLException e) {
            System.out.println("Erro ao buscar solicitação");
            fechar();
            return null;
        }
    }

}
