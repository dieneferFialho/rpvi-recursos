/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.com.rp6.DAO;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;

/**
 *
 * @author Eduardo
 */
public abstract class ConFactory {

    protected static final int MYDB = 0;
    protected static final String Driver = "org.postgresql.Driver";
    protected Connection con;
    protected Statement stm;
    protected ResultSet rs;

    /**
     * Método que realiza a conexão com o banco de dados.
     *
     * @param banco
     * @return
     * @throws ClassNotFoundException
     * @throws SQLException
     */
    public static Connection conexao(int banco) throws ClassNotFoundException, SQLException {

        String url = "jdbc:postgresql://localhost:5432/BD_Almox";
        String usuario = "postgres";
        String senha = "admin";

        switch (banco) {
            case MYDB:
                Class.forName(Driver);
                break;
        }
        return DriverManager.getConnection(url, usuario, senha);
    }

    /**
     * Método que abre a conexão com o banco de dados.
     */
    protected void conectar() throws SQLException {
        try {
            con = ConFactory.conexao(ConFactory.MYDB);
            stm = con.createStatement();

        } catch (ClassNotFoundException e) {
            System.out.println("Erro ao carregar o driver");
        }
    }

    /**
     * Método que fecha a conexão com o banco de dados.
     */
    protected void fechar() {
        try {
            stm.close();
            con.close();
        } catch (SQLException e) {
            System.out.println("Erro ao fechar conexão");
        }
    }
}
