/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.com.rp6.DAO;

import br.com.rp6.model.Produto;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author Eduardo, Michel
 */
public class ProductDAO extends ConFactory {

    /**
     * Método recupera uma lista de Produtos.
     *
     * @param busca string a ser adicionada ao resto do método de busca no banco
     * de dados.
     *
     * @return Retorna uma lista de anúncios encontrados no banco de dados.
     *
     */
    public List<Produto> buscar() {
        List<Produto> lista = new ArrayList<>();
        Produto prod;
        try {
            conectar();
            String sql = "SELECT * FROM produto";
            rs = stm.executeQuery(sql);
            while (rs.next()) {
                prod = new Produto();
                lista.add(prod);
            }
            fechar();
            return lista;
        } catch (SQLException e) {
            System.out.println("Erro ao buscar produto");
            fechar();
            return null;
        }
    }

    /**
     * Método que recupera um Produto.
     *
     * @param descricao string a ser buscada no campo descrição da tabela.
     *
     * @return Retorna um Produto caso encontrado e null caso não o encontre.
     *
     */
    public Produto buscar(String descricao) {
        Produto prod;
        try {
            conectar();
            String sql = "SELECT \n"
                    + "  \"Produto\".codigo, \n"
                    + "  \"Produto\".nome, \n"
                    + "  \"Produto\".descricao\n"
                    + "FROM \n"
                    + "  public.\"Produto\"\n"
                    + "WHERE \"Produto\".nome ='" + descricao + "'";
            rs = stm.executeQuery(sql);
            prod = new Produto();
            fechar();
            return prod;
        } catch (SQLException e) {
            System.out.println("Erro ao buscar produto 2");
            fechar();
            return null;
        }
    }

    public Produto buscar(int cod) {
        Produto prod;
        try {
            conectar();
            String sql = "SELECT * FROM produto WHERE produto.codProduto = '" + cod + "'";
            rs = stm.executeQuery(sql);
            prod = new Produto();
            fechar();
            return prod;
        } catch (SQLException e) {
            System.out.println("Erro ao buscar produto");
            fechar();
            return null;
        }
    }
}
