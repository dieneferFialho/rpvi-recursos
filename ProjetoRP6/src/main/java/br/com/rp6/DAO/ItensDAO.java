/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.com.rp6.DAO;

import br.com.rp6.model.Almoxarifado;
import br.com.rp6.model.Item;
import br.com.rp6.model.Produto;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author eduardo, Michel
 */
public class ItensDAO extends ConFactory {

    public List<Item> buscarItemPorDescricao(String desc) {
        Produto prod;
        Item item;
        Almoxarifado alm;
        List<Item> lista = new ArrayList<>();
        try {
            conectar();
            String sql = "SELECT \n"
                    + "  \"Itens\".\"codigoItem\", \n"
                    + "  \"Itens\".\"codigoProduto\", \n"
                    + "  \"Itens\".\"codigoAlmoxarifado\", \n"
                    + "  \"Itens\".quantidade, \n"
                    + "  \"Itens\".\"quantidadeReservada\", \n"
                    + "  \"Almoxarifado\".\"codAlm\", \n"
                    + "  \"Almoxarifado\".cidade, \n"
                    + "  \"Almoxarifado\".\"codigoAlmoxarife\", \n"
                    + "  \"Produto\".\"codPro\", \n"
                    + "  \"Produto\".nome, \n"
                    + "  \"Produto\".descricao\n"
                    + "FROM \n"
                    + "  public.\"Itens\", \n"
                    + "  public.\"Almoxarifado\", \n"
                    + "  public.\"Produto\"\n"
                    + "WHERE \n"
                    + "  \"Itens\".\"codigoProduto\" = \"Produto\".\"codPro\" AND\n"
                    + "  \"Itens\".\"codigoAlmoxarifado\" = \"Almoxarifado\".\"codAlm\" AND\n"
                    + "   \"Produto\".nome ='" + desc + "';";
            rs = stm.executeQuery(sql);

            while (rs.next()) {

                prod = new Produto();
                item = new Item();
                alm = new Almoxarifado();

                prod.setNome(rs.getString("nome"));
                prod.setDescricao(rs.getString("descricao"));

                alm.setCidade(rs.getString("cidade"));

                item.setProduto(prod);
                item.setAlmoxarifado(alm);
                item.setQuantidade(rs.getInt("quantidade"));
                item.setQuantidadeReservada(rs.getInt("quantidadeReservada"));

                lista.add(item);
            }
            fechar();
            return lista;
        } catch (SQLException e) {
            System.out.println("Erro ao buscar item");
            fechar();
            return null;
        }
    }
    
    
    public List<Item> buscarPorStatus(String desc) {
        Produto prod;
        Item item;
        Almoxarifado alm;
        List<Item> lista = new ArrayList<>();
        try {
            conectar();
            String sql = "SELECT \n"
                    + "  \"Itens\".\"codigoProduto\", \n"
                    + "  \"Itens\".\"codigoAlmoxarifado\", \n"
                    + "  \"Itens\".quantidade, \n"
                    + "  \"Itens\".\"quantidadeReservada\", \n"
                    + "  \"Almoxarifado\".\"codAlm\", \n"
                    + "  \"Almoxarifado\".cidade, \n"
                    + "  \"Almoxarifado\".\"codigoAlmoxarife\", \n"
                    + "  \"Produto\".\"codPro\", \n"
                    + "  \"Produto\".nome, \n"
                    + "  \"Produto\".descricao\n"
                    + "FROM \n"
                    + "  public.\"Itens\", \n"
                    + "  public.\"Almoxarifado\", \n"
                    + "  public.\"Produto\"\n"
                    + "WHERE \n"
                    + "  \"Itens\".\"codigoProduto\" = \"Produto\".\"codPro\" AND\n"
                    + "  \"Itens\".\"codigoAlmoxarifado\" = \"Almoxarifado\".\"codAlm\" AND\n"
                    + "   \"Itens\".status ='" + desc + "';";
            rs = stm.executeQuery(sql);

            while (rs.next()) {

                prod = new Produto();
                item = new Item();
                alm = new Almoxarifado();

                prod.setNome(rs.getString("nome"));
                prod.setDescricao(rs.getString("descricao"));

                alm.setCidade(rs.getString("cidade"));

                item.setProduto(prod);
                item.setAlmoxarifado(alm);
                item.setQuantidade(rs.getInt("quantidade"));
                item.setQuantidadeReservada(rs.getInt("quantidadeReservada"));

                lista.add(item);
            }
            fechar();
            return lista;
        } catch (SQLException e) {
            System.out.println("Erro ao buscar item");
            fechar();
            return null;
        }
    }
    
    

//    
//    public ResultSet buscarItemPorCodigo(int cod) {
//       
//        try {
//            conectar();
//            String sql = "SELECT * FROM produto WHERE produto.codProduto = '" + cod + "'";
//            rs = stm.executeQuery(sql);
//            
//            fechar();
//            return rs;
//        } catch (SQLException ex) {
//            System.out.println("Erro ao buscar item");
//            Logger.getLogger(ItensDAO.class.getName()).log(Level.SEVERE, null, ex);
//            return null;
//        }
//    }
//    
//    
}
