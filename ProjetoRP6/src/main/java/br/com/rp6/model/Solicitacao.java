/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.com.rp6.model;

import br.com.rp6.authentication.User;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;



/**
 *
 * @author Eduardo, Michel
 */
public class Solicitacao {
    
    private Integer numero;
    private Date data; 
    private String status;
    private List<Item> itens = new ArrayList<>();
    private Integer quantidade = 0;
    private Estoque almoDestino;
    private User usuario;

    public Solicitacao() {
    }
    
        

    public Integer getNumero() {
        return numero;
    }

    public void setNumero(Integer numero) {
        this.numero = numero;
    }

    public Date getData() {
        return data;
    }

    public void setData(Date data) {
        this.data = data;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public List<Item> getItens() {
        return itens;
    }

    public void setItens(List<Item> itens) {
        this.itens = itens;
    }   

    public Estoque getAlmoDestino() {
        return almoDestino;
    }

    public void setAlmoDestino(Estoque almoDestino) {
        this.almoDestino = almoDestino;
    }

    public User getUsuario() {
        return usuario;
    }

    public void setUsuario(User usuario) {
        this.usuario = usuario;
    }

    public Integer getQuantidade() {
        return quantidade;
    }

    public void setQuantidade(Integer quantidade) {
        this.quantidade = quantidade;
    }

 
    
}
