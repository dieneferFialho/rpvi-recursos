/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.com.rp6.model;

/**
 *
 * @author Eduardo, Michel
 */
public class Produto {
    
    private String descricao;
    private String nome;

    public Produto() {
    }

    
    
    public Produto(String descricao, String nome) {
        this.descricao = descricao;
        this.nome = nome;
    }

    
    
    
    
    public String getDescricao() {
        return descricao;
    }

    public void setDescricao(String descricao) {
        this.descricao = descricao;
    }

    public String getNome() {
        return nome;
    }

    public void setNome(String nome) {
        this.nome = nome;
    }
    
    
    
  
}
