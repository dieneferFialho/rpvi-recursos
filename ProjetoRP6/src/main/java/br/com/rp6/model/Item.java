/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.com.rp6.model;



/**
 *
 * @author MichelM
 */
public class Item {
    
    private Integer codigo;
    private Almoxarifado almoxarifado;
    private Produto produto;
    private int quantidade;
    private int quantidadeReservada;
    
    public Item() {
    }

    public Item(Almoxarifado almoxarifado, Produto produto, int quantidade, int quantidadeReservada) {
        this.almoxarifado = almoxarifado;
        this.produto = produto;
        this.quantidade = quantidade;
        this.quantidadeReservada = quantidadeReservada;
    }

    

    public Produto getProduto() {
        return produto;
    }

    public void setProduto(Produto produto) {
        this.produto = produto;
    }

    public int getQuantidade() {
        return quantidade;
    }

    public void setQuantidade(int quantidade) {
        this.quantidade = quantidade;
    }

    public int getQuantidadeReservada() {
        return quantidadeReservada;
    }

    public void setQuantidadeReservada(int quantidadeReservada) {
        this.quantidadeReservada = quantidadeReservada;
    }

    public Almoxarifado getAlmoxarifado() {
        return almoxarifado;
    }

    public void setAlmoxarifado(Almoxarifado almoxarifado) {
        this.almoxarifado = almoxarifado;
    }

    public Integer getCodigo() {
        return codigo;
    }

    public void setCodigo(Integer codigo) {
        this.codigo = codigo;
    }

    
    
    

    
    
    
}
