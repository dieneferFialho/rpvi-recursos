/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.com.rp6.authentication;

import br.com.rp6.DAO.UserDAO;
import javax.faces.context.FacesContext;
import javax.servlet.http.HttpSession;

/**
 *
 * @author eduarodo
 */
public class User {

    private Integer codigo;
    private String nome;
    private Integer login;
    private String senha;
    private final UserDAO usuarioDAO;
    public static final String USUARIO_LOGADO = "usuarioLogado";
    private Integer codAlm;

    public User() {
        this.usuarioDAO = new UserDAO();
    }

    public boolean ValidaUsuario(Integer log, String password) {
       
        User usuarioLogado = usuarioDAO.getUsuario(log, password);

        if (usuarioLogado != null) {
            HttpSession session = (HttpSession) FacesContext.getCurrentInstance().getExternalContext().getSession(true);
            session.setAttribute(USUARIO_LOGADO, usuarioLogado);
            return true;
        } else {
            return false;
        }

    }
    
    public User getUsuarioLogado() {
	User usuarioSessao = (User) ((HttpSession) FacesContext.getCurrentInstance()
								
				.getExternalContext().getSession(false)).getAttribute(USUARIO_LOGADO);
		
		return usuarioSessao;
                
	}

    public Integer getCodigo() {
        return codigo;
    }

    public void setCodigo(Integer codigo) {
        this.codigo = codigo;
    }  
    
    public String getNome() {
        return nome;
    }

    public void setNome(String nome) {
        this.nome = nome;
    }

    public Integer getLogin() {
        return login;
    }

    public void setLogin(Integer login) {
        this.login = login;
    }

    public String getSenha() {
        return senha;
    }

    public void setSenha(String senha) {
        this.senha = senha;
    }

    public static String getUSUARIO_LOGADO() {
        return USUARIO_LOGADO;
    }

    public Integer getCodAlm() {
        return codAlm;
    }

    public void setCodAlm(Integer codAlm) {
        this.codAlm = codAlm;
    }

    
    
    
}
