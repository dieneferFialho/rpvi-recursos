/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.com.rp6.bean;

import javax.faces.bean.ManagedBean;

/**
 *
 * @author eduarodo
 */
@ManagedBean
public class BeanControlePainel {
    
    public String interno(){
        return "interno";
    }
    
    
    public String buscar(){
        return "busca";
    }
    
    public String solicitar(){
        return "solicitarItens";
    }
    
    public String receberItens(){
        return "receberItens";
    }
    
    public String analisar(){
        return "analisarSolicitacoes";
    }
    
    public String paginaTeste(){
        return "paginateste";
    }
}
