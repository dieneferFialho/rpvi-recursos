/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.com.rp6.bean;

import br.com.rp6.DAO.SolicitacaoDAO;
import br.com.rp6.model.Item;
import br.com.rp6.model.Produto;
import br.com.rp6.model.Solicitacao;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import javax.faces.application.FacesMessage;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ViewScoped;
import javax.faces.context.FacesContext;
import org.primefaces.event.RowEditEvent;

/**
 *
 * @author eduardo, michel
 */
@ManagedBean

public class BeanSolicitacao implements Serializable {

    private Solicitacao solicitacao;
    private ArrayList<Solicitacao> solicitados = new ArrayList<>();
    private SolicitacaoDAO dao = new SolicitacaoDAO();
    private Produto pro;  
    private Integer cod;
    private String desc;
    private String camp;
    private Integer quant;
       
    


    public void enviarSolicitacao() {

    }


    public void buscarSolicitacoes() {
        solicitados = dao.buscarSolicitacoes();
    }

    public Solicitacao getSolicitacao() {
        return solicitacao;
    }

    public void setSolicitacao(Solicitacao solicitacao) {
        this.solicitacao = solicitacao;
    }

    public ArrayList<Solicitacao> getSolicitados() {
        return solicitados;
    }

    public void setSolicitados(ArrayList<Solicitacao> solicitados) {
        this.solicitados = solicitados;
    }

    public Integer getCod() {
        return cod;
    }

    public String getDesc() {
        return desc;
    }

    public void setDesc(String desc) {
        this.desc = desc;
    }

    public String getCamp() {
        return camp;
    }

    public void setCamp(String camp) {
        this.camp = camp;
    }

    public Integer getQuant() {
        return quant;
    }

    public void setQuant(Integer quant) {
        this.quant = quant;
    }

    public Produto getPro() {
        return pro;
    }

    public void setPro(Produto pro) {
        this.pro = pro;
    }
    
    
    

//    public Almoxarifado getAlm() {
//        return alm;
//    }
//
//    public void setAlm(Almoxarifado alm) {
//        this.alm = alm;
//    }

    
}
