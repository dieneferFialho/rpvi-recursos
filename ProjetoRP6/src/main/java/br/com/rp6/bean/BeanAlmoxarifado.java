/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.com.rp6.bean;

import br.com.rp6.DAO.EstoqueDAO;
import br.com.rp6.model.Estoque;
import java.util.ArrayList;
import javax.faces.bean.ManagedBean;

/**
 *
 * @author eduarodo
 */
@ManagedBean
public class BeanAlmoxarifado {
    
    private ArrayList<Estoque> itens = new ArrayList<> ();
    private Estoque estoque;
    private EstoqueDAO esto = new EstoqueDAO();
    private String ProdutoDaBusca ;
    
    
   
    
     public void pesquisar (){
         itens = estoque.buscarTodos(ProdutoDaBusca);       
    }

          
     
    public ArrayList<Estoque> getItens() {
        return itens;
    }

    public void setItens(ArrayList<Estoque> itens) {
        this.itens = itens;
    }

    public Estoque getEstoque() {
        return estoque;
    }

    public void setEstoque(Estoque estoque) {
        this.estoque = estoque;
    }

    public EstoqueDAO getEsto() {
        return esto;
    }

    public void setEsto(EstoqueDAO esto) {
        this.esto = esto;
    }

    public String getProdutoDaBusca() {
        return ProdutoDaBusca;
    }

    public void setProdutoDaBusca(String ProdutoDaBusca) {
        this.ProdutoDaBusca = ProdutoDaBusca;
    }

   
    
    
    
}
