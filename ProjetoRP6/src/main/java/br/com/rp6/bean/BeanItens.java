/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.com.rp6.bean;

import br.com.rp6.DAO.ItensDAO;
import br.com.rp6.DAO.SolicitacaoDAO;
import br.com.rp6.authentication.User;
import br.com.rp6.model.Item;
import br.com.rp6.model.Solicitacao;

import java.io.Serializable;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import javax.faces.application.FacesMessage;
import javax.faces.bean.ManagedBean;

import javax.faces.bean.ViewScoped;
import javax.faces.context.FacesContext;
import org.primefaces.event.RowEditEvent;

/**
 *
 * @author eduarodo
 */
@ManagedBean
@ViewScoped

public class BeanItens implements Serializable {

    private final ItensDAO dao = new ItensDAO();
    private final SolicitacaoDAO daosoli = new SolicitacaoDAO();
    private List<Item> lista = new ArrayList();
    private List<Item> itensDaSolicitacao = new ArrayList();
    private String produtoDaBusca;
    private Item itemse = new Item();
    private Solicitacao solicitacao;
    private Integer quantidadeRequisitada = 0;
    private List<Solicitacao> soli = new ArrayList();
    private User user = new User () ;

    public BeanItens() {

    }

    public void onRowEdit(RowEditEvent event) {

        FacesMessage msg = new FacesMessage("Quantidade adicionada", produtoDaBusca);
        FacesContext.getCurrentInstance().addMessage(null, msg);
    }

    public void onRowCancel(RowEditEvent event) {
        FacesMessage msg = new FacesMessage("Edição cancelada", produtoDaBusca);
        FacesContext.getCurrentInstance().addMessage(null, msg);
    }

    public void buscaTodosOsItens() {
        lista = dao.buscarItemPorDescricao(produtoDaBusca);
    }

    public Date retornaData(){
       Date data = new Date(System.currentTimeMillis());  
       SimpleDateFormat formatarDate = new SimpleDateFormat("yyyy-MM-dd");
       formatarDate.format( data );
       return data;
    }
    
    public void enviaSolicitacao() {
        
        String status ="solicitado";
        Integer numero = 11;
        
        User user2 = new User();
        user2 = user.getUsuarioLogado();
        
        solicitacao = new Solicitacao();        
        solicitacao.setNumero(numero);
        solicitacao.setData(retornaData());
        solicitacao.setStatus(status);
        solicitacao.setItens(itensDaSolicitacao);
        solicitacao.setQuantidade(quantidadeRequisitada);
        solicitacao.setUsuario(user2);
        
        System.out.println(user2.getCodigo());
        
        
        daosoli.salvar(solicitacao);
        
       FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_ERROR, "Cadastrado", "ok"));
    }

    public List<Item> getLista() {
        return lista;
    }

    public String getProdutoDaBusca() {
        return produtoDaBusca;
    }

    public void setProdutoDaBusca(String produtoDaBusca) {
        this.produtoDaBusca = produtoDaBusca;
    }

    public Item getItemse() {
        return itemse;
    }

    public void setItemse(Item itemse) {
        this.itemse = itemse;
        addSoli();
    }

    public void addSoli() {
        itensDaSolicitacao.add(itemse);
    }

    public Solicitacao getSolicitacao() {
        return solicitacao;
    }

    public void setSolicitacao(Solicitacao solicitacao) {
        this.solicitacao = solicitacao;

    }

    public List<Solicitacao> getSoli() {
        return soli;
    }

    public void setSoli(List<Solicitacao> soli) {
        this.soli = soli;

    }

    public List<Item> getItensDaSolicitacao() {
        return itensDaSolicitacao;
    }

    public void setItensDaSolicitacao(List<Item> itensDaSolicitacao) {
        this.itensDaSolicitacao = itensDaSolicitacao;
    }

    public Integer getQuantidadeRequisitada() {
        return quantidadeRequisitada;
    }

    public void setQuantidadeRequisitada(Integer quantidadeRequisitada) {
        this.quantidadeRequisitada = quantidadeRequisitada;
    }

    public User getUser() {
        return user;
    }

    public void setUser(User user) {
        this.user = user;
    }
    
    
    

}
