/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.com.rp6.bean;

import br.com.rp6.authentication.User;
import javax.faces.application.FacesMessage;
import javax.faces.bean.ManagedBean;
import javax.faces.context.FacesContext;

/**
 *
 * @author eduarodo
 */
@ManagedBean
public class BeanLoginUser {

    private Integer log;
    private String password;
    private User user = new User();
    private User usuarioLogado;

    public BeanLoginUser() {
    }

    public String logar() {
        boolean resposta;
        resposta = user.ValidaUsuario(log, password);

        if (resposta == true) {
            if (getUsuarioLogado().getCodAlm() != 4) {
                return "interno";
            }else{
                return "internoCentral";
            }
                   
                    
        } else {
            FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_ERROR, "Usuário não existe! Erro no Login!", "Erro no Login!"));
            return null;
        }
    }
 

    public User getUsuarioLogado() {
		usuarioLogado = user.getUsuarioLogado();
		
		return usuarioLogado;
                
	}
    
    

    public void setUsuarioLogado(User usuarioLogado) {
        this.usuarioLogado = usuarioLogado;
    }
    
   

    public Integer getLog() {
        return log;
    }

    public void setLog(Integer log) {
        this.log = log;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public void setUser(User user) {
        this.user = user;
    }

    public User getUser() {
        return user;
    }

}
