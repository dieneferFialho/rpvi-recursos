/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.com.rp6.bean;

import br.com.rp6.DAO.ProductDAO;
import java.util.ArrayList;
import javax.faces.bean.ManagedBean;
import br.com.rp6.model.Produto;
import java.sql.SQLException;
import java.util.List;
/**
 *
 * @author 131151943
 */
@ManagedBean
public class BeanProduto {
    
    private ProductDAO produto = new ProductDAO();    
    private ArrayList<Produto> produtos = new ArrayList();
    private String criterio;

    
    
//    public void pesquisar (){
//        produtos = (ArrayList<Produto>) produto.buscar(criterio);
//    }
    
    
    public List<Produto> getProdutos()  {
               return produtos;
    }

    public void setProdutos(ArrayList<Produto> produtos) {
        this.produtos = produtos;
    }
    
    public ProductDAO getProduto() {
        return produto;
    }

    public void setProduto(ProductDAO produto) {
        this.produto = produto;
    }

    public String getCriterio() {
        return criterio;
    }

    public void setCriterio(String criterio) {
        this.criterio = criterio;
    }
                   
}
