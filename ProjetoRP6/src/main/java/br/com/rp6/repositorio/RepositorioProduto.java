/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.com.rp6.repositorio;

import br.com.rp6.DAO.ProductDAO;
import br.com.rp6.model.Produto;
import java.sql.ResultSet;

/**
 *
 * @author MichelM
 */
public class RepositorioProduto {
    ProductDAO daoProduto = new ProductDAO();
    ResultSet rs;

    public Produto buscaPorID(int id) {
        return daoProduto.buscar(id);
    }

}
